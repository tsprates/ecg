package com.github.tsprates.ecg.beans;

public class Amostra {

    private final int numero;

    private final int frequenciaCardiaca;

    private final int ganho;

    private final int canalRitmo;

    private final int[] pontos;

    public Amostra(int numero, int frequenciaCardiaca, int ganho, int canalRitmo, int[] pontos) {
	this.numero = numero;
	this.frequenciaCardiaca = frequenciaCardiaca;
	this.ganho = ganho;
	this.canalRitmo = canalRitmo;
	this.pontos = pontos;
    }

    public int getNumero() {
	return numero;
    }

    public int getFrequenciaCardiaca() {
	return frequenciaCardiaca;
    }

    public int getGanho() {
	return ganho;
    }

    public int getCanalRitmo() {
	return canalRitmo;
    }

    public int[] getPontos() {
	return pontos;
    }
}
