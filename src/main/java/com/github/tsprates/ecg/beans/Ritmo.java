package com.github.tsprates.ecg.beans;

public class Ritmo {

    private final int quantidade;

    private final String taxaAmostragem;

    private final String sensibilidade;

    private final int amostrasPorBloco;

    public Ritmo(int quantidade, String taxaAmostragem, String sensibilidade, int amostrasPorBloco) {
	this.quantidade = quantidade;
	this.taxaAmostragem = taxaAmostragem.substring(0, taxaAmostragem.indexOf(" "));
	this.sensibilidade = sensibilidade;
	this.amostrasPorBloco = amostrasPorBloco;
    }

    public int getQuantidade() {
	return quantidade;
    }

    public String getTaxaAmostragem() {
	return taxaAmostragem;
    }

    public String getSensibilidade() {
	return sensibilidade;
    }

    public int getAmostrasPorBloco() {
	return amostrasPorBloco;
    }

}
