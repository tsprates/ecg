package com.github.tsprates.ecg.utils;

import java.awt.Image;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

public final class Grafico {

    private int largura;

    private int altura;

    private JFreeChart chart;

    /**
     * Construtor.
     *
     * @param largura Largura da imagem do gráfico.
     * @param altura Altura da imagem do gráfico.
     */
    public Grafico(int largura, int altura) {
        setLarguraEAltura(largura, altura);
    }

    /**
     * Seta altura e largura do gráfico.
     *
     * @param largura Largura da imagem do gráfico.
     * @param altura Altura da imagem do gráfico.
     */
    public void setLarguraEAltura(int largura, int altura) {
        this.largura = largura;
        this.altura = altura;
    }

    /**
     * Plota gráfico.
     *
     * @param titulo String com título do gráfico.
     * @param pontos Array de inteiros referente aos pontos da amostra.
     * @return Imagem contendo o gráfico gerado.
     */
    public Image plota(String titulo, int[] pontos) {
        chart = ChartFactory.createXYLineChart(titulo,
                                               "Tempo (s)",
                                               "Voltagem (volts)",
                                               criaDataset(pontos));
        chart.removeLegend();
        return chart.createBufferedImage(largura, altura);
    }

    private static XYDataset criaDataset(int[] pontos) {
        XYSeries serie = new XYSeries("ECG");

        for (int i = 0, len = pontos.length; i < len; i++) {
            serie.add(i, pontos[i]);
        }

        XYSeriesCollection collection = new XYSeriesCollection();
        collection.addSeries(serie);
        return collection;
    }
}
