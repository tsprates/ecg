package com.github.tsprates.ecg.utils;

import com.github.tsprates.ecg.beans.Amostra;
import com.github.tsprates.ecg.beans.Ritmo;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Wxml {

    private final Document doc;

    private final List<Amostra> amostras = new ArrayList<>();

    private Ritmo ritmo;

    /**
     * Construtor.
     *
     * @param wxml Arquivo .WXML
     */
    public Wxml(File wxml) {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        
        try {
            builder = factory.newDocumentBuilder();
            doc = builder.parse(wxml);
            doc.normalizeDocument();
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Lista de amostras.
     *
     * @return Lista de amostras {@link Amostra}.
     */
    public List<Amostra> getAmostras() {
        return amostras;
    }

    /**
     * Realiza leitura do arquivo .WXML.
     */
    public void carregaAmostras() {
        NodeList blocos = doc.getDocumentElement().getElementsByTagName("Blocos");

        Element blocosElement = (Element) blocos.item(0);
        NodeList blocoNodeList = blocosElement.getElementsByTagName("Bloco");

        amostras.clear();

        for (int i = 0; i < blocoNodeList.getLength(); i++) {
            Node node = blocoNodeList.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                String numero = node.getAttributes().getNamedItem("Numero").getNodeValue();

                NodeList nodeList = node.getChildNodes();
                String frequenciaCardiaca = nodeList.item(0).getTextContent();
                String ganho = nodeList.item(1).getTextContent();
                String canalRitmo = nodeList.item(2).getTextContent();
                String strPontos = nodeList.item(5).getTextContent();

                Amostra amostra = new Amostra(Integer.parseInt(numero),
                                              Integer.parseInt(frequenciaCardiaca),
                                              Integer.parseInt(ganho),
                                              Integer.parseInt(canalRitmo),
                                              toArray(strPontos));

                amostras.add(amostra);
            }
        }

        NamedNodeMap ritmoAtributos = blocosElement.getParentNode().getAttributes();

        ritmo = new Ritmo(Integer.parseInt(ritmoAtributos.getNamedItem("Quantidade").getNodeValue()),
                          ritmoAtributos.getNamedItem("TaxaAmostragem").getNodeValue(),
                          ritmoAtributos.getNamedItem("Sensibilidade").getNodeValue(),
                          Integer.parseInt(ritmoAtributos.getNamedItem("AmostrasPorBloco").getNodeValue()));
    }

    private int[] toArray(String strPts) {
        String[] arrPts = strPts.split(";");
        int[] pts = new int[arrPts.length];
        
        for (int i = 0; i < arrPts.length; i++) {
            pts[i] = Integer.parseInt(arrPts[i]);
        }

        return pts;
    }

    /**
     * Retorna informação do ritmo {@link Ritmo}.
     *
     * @return Classe Ritmo.
     */
    public Ritmo getRitmo() {
        return ritmo;
    }
}
